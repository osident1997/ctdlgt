import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class InsertionSort1 {
    
    

    public static void insertIntoSorted(int[] ar) {
        // Fill up this function
        int s = ar.length;
        int a = ar[s-1];
        for(int i = s-2;i>=0;i--){
            if(ar[i]>a){
                ar[i+1]=ar[i];
            }
            else if(ar[i]<a){
                ar[i+1]=a;
                i=-1;
            }
            for(int j=0;j<s;j++){
                System.out.print(ar[j]+"\t");
            }
            System.out.println();
        }
        if(ar[0]>a){
            ar[0]=a;
            for(int j=0;j<s;j++){
                System.out.print(ar[j]+"\t");
            }
        }
    }
    
    
/* Tail starts here */
     public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int s = in.nextInt();
        int[] ar = new int[s];
         for(int i=0;i<s;i++){
            ar[i]=in.nextInt(); 
         }
         insertIntoSorted(ar);
    }
    
    
    private static void printArray(int[] ar) {
      for(int n: ar){
         System.out.print(n+" ");
      }
        System.out.println("");
   }
    
    
}
