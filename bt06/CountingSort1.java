import java.io.*;
import java.util.*;

public class CountingSort1 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[] a = new int[n];
        for(int i=0;i<n;i++){
            a[i]=s.nextInt();
        }
        int[] dem=new int[100];
        for(int i=0;i<100;i++){
            dem[i]=0;
        }
        for(int i=0;i<n;i++){
            for(int j=0;j<100;j++){
                if(a[i]==j)
                    dem[j]++;
            }
        }
        for(int i=0;i<100;i++){
            System.out.print(dem[i]+" ");
        }
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
    }
}