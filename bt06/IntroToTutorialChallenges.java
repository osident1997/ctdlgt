import java.io.*;
import java.util.*;

public class IntroToTutorialChallenges {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner s=new Scanner(System.in);
        int V = s.nextInt();
        int n = s.nextInt();
        int[] a = new int[n];
        for(int i = 0;i<n;i++){
            a[i] = s.nextInt();
        }
        int lo = 0;
        int hi = n - 1;
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
            if      (V < a[mid]) hi = mid - 1;
            else if (V > a[mid]) lo = mid + 1;
            else {
                System.out.println(mid);
                break;}
        }
    }
}