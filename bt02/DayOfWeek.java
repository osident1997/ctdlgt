public class DayOfWeek{
	public static void main(String[]args){
		int d = Integer.parseInt(args[0]);
		int m = Integer.parseInt(args[1]);
		int y = Integer.parseInt(args[2]);
		int x,d0,m0,y0;
		y0 = (int)(y - (14 - m) / 12);
		x = (int)(y0 + y0/4 - y0/100 + y0/400);
		m0 = (int)(m + 12 * ((14 - m) / 12) - 2);
		d0 = (int)((d + x + (31*m0)/ 12) % 7);
		System.out.println(d0);
	}
}