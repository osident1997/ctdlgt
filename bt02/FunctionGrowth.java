public class FunctionGrowth{
	public static void main(String[]args){
		System.out.println("N\tlog2N\tNlog2N\tN^2\tN^3");
		for(int N=16;N<=2048;N*=2){
			System.out.println(N+"\t"+(int)Math.log(N)+"\t"+(int)(N*Math.log(N))+"\t"+N*N+"\t"+N*N*N);
		}
		
	}
}