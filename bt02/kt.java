public class kt{
	public static void main(String[]args){
		double degrees = Double.parseDouble(args[0]); 
		double radians = Math.toRadians(degrees); 
		double s=Math.sin(radians);
		double c=Math.cos(radians);
		System.out.println(s*s+c*c);
	}
}