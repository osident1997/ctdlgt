public class Solution{
	public static int getHeight(Node root){
        if(root == null) return -1;
        else return (int) (Math.max(getHeight(root.left), getHeight(root.right)) + 1);
    }
	static Node Insert(Node root,int value)
    {
     if(root==null)
     {
        Node node=new Node();
        node.data=value;
        node.left=null;
        node.right=null;
        root=node;
    }
    else if(root.data>value)
        root.left=Insert(root.left,value);
    else if(root.data<value)
        root.right=Insert(root.right,value);

    return root;
}
}