import java.io.*;
import java.util.*;

public class Solution {
    public static int factorial(int n) {
        if(n==1||n==2){
            return n;
        }
        else return (factorial(n-1)*n);
    }
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner s = new Scanner(System.in);
        int N = s.nextInt();
        System.out.println(factorial(N));
    }
}