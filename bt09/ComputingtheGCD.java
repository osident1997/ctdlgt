import java.io.*;
import java.util.*;

public class ComputingtheGCD {
    public static int gcd(int m, int n){
    if(m%n == 0)
        return n;
    else
        return gcd(n, m%n);
}
    public static void main(String[] args) {
    int x;
    int y;
    Scanner scan = new Scanner(System.in);
    x = scan.nextInt();
    y = scan.nextInt();
    System.out.println(gcd(x, y));
}
    }