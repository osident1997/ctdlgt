import java.io.*;
import java.util.*;

public class FindtheMedian {
    public static int[] a;
    public static int[] c = new int[20001];
    public static int n;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        a = new int[n];
        for(int i = 0; i < n; i++){
            a[i] = in.nextInt();
            c[a[i]+10000]++;
        }
        int s =0;
        outer:
        for(int i = 0; i < 20001; i++){
            s+=c[i];
            if(s>n/2){
                System.out.print(i-10000);
                break outer;
            }
        }
    }
}