import java.io.*;
import java.util.*;

public class JesseandCookies {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanner = new Scanner(System.in);
        int N, K;
        N = scanner.nextInt();
        K = scanner.nextInt();
        
        Queue q = new PriorityQueue(N);
        for (int i = 0; i < N; i++) {
            q.add(scanner.nextInt());
        }
        int j=0;
        while(true) {
            if((int)q.peek()>=K){
                System.out.println(j);
                break;
            }
            else if(q.size()>1){
                int a = (int)q.remove();
                int b = (int)q.remove();
                q.add(a+2*b);
                j++;
            }
            else{
                System.out.println("-1");
                break;
            }
        }

}
}