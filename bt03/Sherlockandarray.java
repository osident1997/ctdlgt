import java.io.*;
import java.util.*;

public class Sherlockandarray {

    public static void main(String[] args) {
         Scanner s=new Scanner(System.in);
        int T=s.nextInt();
        for(int i=0;i<T;i++) {
            int N=s.nextInt();
            int sum=0;
            int[] a=new int[N];
            for(int j=0;j<N;j++) {
                 a[j]=s.nextInt();
                sum+=a[j];
            }
            int c=0;
            for(int j=0;j<N;j++){
                int sum1=0,sum2=0;
                if(j==0){
                        sum2=sum-a[j];
                }
                else if(j==N-1){
                        sum1=sum-a[N-1]; 
                }
                    else{
                        for(int k=0;k<j;k++){
                            sum1+=a[k];
                            sum2=sum-a[j]-sum1;
                        }
                    }
                if(sum1==sum2){
                    c=1;
                    break;
                }
            }
            if(c==1)
                System.out.println("YES");
            else
                System.out.println("NO");
        }
    }
}