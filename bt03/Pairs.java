import java.io.*;
import java.util.*;
import java.util.Scanner;
import java.util.Arrays;
public class Pairs {

    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        int N = s.nextInt();
        int K = s.nextInt();
        int[] a=new int[N];
        for(int i=0;i<N;i++){
            a[i]=s.nextInt();
        }
        Arrays.sort(a);
        int dem=0;
        for(int i=0;i<N;i++){
            for(int j=i+1;j<N;j++){
                if(a[j]-a[i]==K){
                    dem++;
                }
                else if(a[j]-a[i]>K){
                    j=N+1;
                }
            }
        }
        System.out.println(dem);
    }
}