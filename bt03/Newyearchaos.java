import java.io.*;
import java.util.*;

public class Newyearchaos {

    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        int T=s.nextInt();
        for(int i=0;i<T;i++){
            int n=s.nextInt();
            int[] a=new int[n];
            for(int j=0;j<n;j++){
                a[j]=s.nextInt();
            }
            int dem=0;
            for(int j=0;j<n;j++){
                if(a[j]-j>=4){
                    System.out.println("Too chaotic");
                    dem=0;
                    break;
                }
                else if(j<(a[j]-1) && j-a[j]<4){
                    dem+=(a[j]-j-1);
                }
            }
            if(dem!=0)
            System.out.println(dem);
        }
    }
}