import java.util.Arrays;
import edu.princeton.cs.algs4.Stopwatch;
public class BinarySearch{
	public static int binarySearch(int[] a,int key)
	{
	int lo = 0, hi = a.length-1;
	while (lo <= hi)
	{
	int mid = lo + (hi - lo) / 2;
	if (key < a[mid]) hi = mid - 1;
	else if (key > a[mid]) lo = mid + 1;
	else return mid;
	}
	return -1;
	}
    public static int rank(int key, int[] a) {
        return binarySearch(a, key);
    }
	 public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
        int[] a = new int[N];
		for(int i=0;i<N;i++){
			a[i]=(int)(Math.random()*N);
			System.out.print(a[i]+"\t");
		}
		System.out.println();
        Arrays.sort(a);
		for(int i=0;i<N;i++){
			System.out.print(a[i]+"\t");
		}
		while (!StdIn.isEmpty()) {
            int key = StdIn.readInt();
            if(BinarySearch.binarySearch(a,key)==-1){
				System.out.println(-1);
			}
			else{
				for(int i=0;i<N;i++){
					if(a[i]==a[BinarySearch.binarySearch(a,key)])
						System.out.print(i+"\t");
				}
				System.out.println();
			}
        }
		
    }

}