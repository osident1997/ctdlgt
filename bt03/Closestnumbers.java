import java.io.*;
import java.util.*;

public class Closestnumbers {

    public static void main(String[] args) {
         Scanner s = new Scanner(System.in);
        int N= s.nextInt();
        int[] a=new int[N];
        for(int i=0;i<N;i++){
            a[i]=s.nextInt();
        }
       Arrays.sort(a);
        int min=a[1]-a[0];
        for(int i=0;i<N-1;i++){
                if((a[i+1]-a[i])<min)
                    min=a[i+1]-a[i];
            }
             for(int i=0;i<N-1;i++){
                if((a[i+1]-a[i])==min){
                    System.out.print(a[i]+" "+a[i+1]+" ");
                }
            }
    }
}