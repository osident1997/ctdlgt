public class LinkedList{
void Print(Node head) {
    if(head==null || head.next==null)
    return;
    else{
        Node temp = head;
        while(temp!=null){
        System.out.println(temp.data);
        temp = temp.next;
    }  
}
  
}

Node Insert(Node head,int data) {
// This is a "method-only" submission. 
// You only need to complete this method.
    Node tmp = new Node();
    tmp.data = data; 
    tmp.next = null;
    if(head == null) {
    head = tmp;
    return head;
} 

Node current = head;
while(current.next != null) {
    current = current.next;
}
current.next = tmp;
return head;
  
}


Node InsertNth(Node head, int data, int position) {
    Node newN=new Node();
    newN.data=data;
    Node head1=new Node();
    head1=head;
    if(head==null){
        head=newN;
        return newN;
    }
    if(position==0){
       newN.next=head;
       return newN;
    }
    Node current=new Node();
    current=head;
    for (int i=0; i < position-1 && current.next !=null; i++) {
        current = current.next;
    }
    if (current.next != null) {
        newN.next =  current.next;
        current.next = newN;
    } 
    else {
        current.next = newN;
    }
    return head;
    }
	
	Node Delete(Node head, int position) {
  // Complete this method
  if(position ==0){
        return head.next;
    }else if(head == null){
        return head;
    }
    Node current = head;
    for(int i=0;i<position-1;i++){
        current = current.next;
    }
          
    current.next = current.next.next;
    return head;
}

void ReversePrint(Node head) {
  // This is a "method-only" submission. 
  // You only need to complete this method. 
    if(head == null)
        ;
    else{
        ReversePrint(head.next);
        System.out.println(head.data);
}
    
}

Node Reverse(Node head) {
    if (head == null || head.next == null) {  
    return head;  
}
    Node remain = Reverse(head.next);
    head.next.next = head; 
    head.next = null; 
    return remain; 
}

int CompareLists(Node headA, Node headB) {
    // This is a "method-only" submission. 
    // You only need to complete this method 
    if(headA==null&&headB==null)
        return 1;
    else if(headA==null||headB==null)
        return 0;
    for(int i=0;headA.next!=null&&headB.next!=null;i++){
        if(headA.data!=headB.data){
            return 0;
        }
        else{
            headA=headA.next;
            headB=headB.next;
        }
    }
    if(headA.next==null&&headB.next==null)
        return 1;
    return 0;
}

Node MergeLists(Node headA, Node headB) {
     // This is a "method-only" submission. 
     // You only need to complete this method 
   if((headA==null)&&(headB==null))
    return null;
    if((headA!=null)&&(headB==null))
    return headA;
    if((headA == null)&&(headB!=null))
    return headB;
    if(headA.data < headB.data)
    headA.next = MergeLists(headA.next, headB);
    else if(headA.data > headB.data)
        {
        Node temp = headB;
        headB = headB.next;
        temp.next = headA;
        headA = temp;
        headA.next = MergeLists(headA.next, headB);
}
return headA;

}

int GetNode(Node head,int n) {
     // This is a "method-only" submission. 
     // You only need to complete this method. 
    int dem = 0;
    Node current = head;
    Node result = head;
    while(current!=null)
    {
        current=current.next;
        if (dem++>n)
        {
            result=result.next;
        }
    }
    return result.data;

}
}