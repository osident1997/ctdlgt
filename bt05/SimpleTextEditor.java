import java.io.*;
import java.util.*;

public class SimpleTextEditor {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
    Scanner scan = new Scanner(System.in);
    int n = scan.nextInt();
    String S = "";
    int i = 0;
    Stack<String> stack = new Stack<String>();
    while (n-- > 0) {
        i = scan.nextInt();
        switch(i) {
            case 1 : 
                       stack.push(S); 
                       S = S + scan.next();
                        break;
            case 2 : 
                       i = scan.nextInt();
                       stack.push(S);
                       S = S.substring(0, S.length()-i); 
                       break;
            case 3 :
                        i = scan.nextInt();
                        System.out.println(S.charAt(i-1));
                        break;
            case 4 :
                      S = stack.pop();
        }

    }
    }
}