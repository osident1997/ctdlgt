import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class EqualStacks {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n1 = in.nextInt();
        int n2 = in.nextInt();
        int n3 = in.nextInt();
        int h1[] = new int[n1];
        int sum1[] = new int[n1];
        int sum=0;
        for(int k=0; k < n1; k++){
            h1[k] = in.nextInt();
            sum+=h1[k];
            sum1[k]=sum;
        }
        int h2[] = new int[n2];
        int sum2[] = new int[n2];
        sum=0;
        for(int h2_i=0; h2_i < n2; h2_i++){
            h2[h2_i] = in.nextInt();
            sum+=h2[h2_i];
            sum2[h2_i]=sum;
        }
        int h3[] = new int[n3];
        int sum3[] = new int[n3];
        sum=0;
        for(int h3_i=0; h3_i < n3; h3_i++){
            h3[h3_i] = in.nextInt();
            sum+=h3[h3_i];
            sum3[h3_i]=sum;
        }
    int col1 = n1-1; 
    int col2 = n2-1;
    int col3 = n3-1; 

    while( sum1[col1] != sum2[col2] || sum2[col2] != sum3[col3] ) 
    {         
        if( sum1[col1] > sum2[col2] || sum1[col1] > sum3[col3] )
            col1--;
        else if( sum2[col2] > sum1[col1] || sum2[col2] > sum3[col3] )
            col2--;
        else if( sum3[col3] > sum2[col2] || sum3[col3] > sum1[col1])
            col3--;
    }

    System.out.println(sum1[col1]);
    }
}
