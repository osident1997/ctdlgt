import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class BalancedBrackets {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
    int t = in.nextInt();
    for(int a0 = 0; a0 < t; a0++){
        String s1 = in.next();
        char s[]=s1.toCharArray();
        boolean result=true;
        Stack<Character> st=new Stack<Character>();
        for(int i=0;i<s.length;i++)
            {
            if(s[i]=='('||s[i]=='{'||s[i]=='[')
                {
                st.push(s[i]);
            }
            if(s[i]==')')
                {
                if(st.empty()||st.peek()!='('){
                    result=false;
                    break;
                }
                st.pop();
            }
            else if(s[i]=='}')
                {
                if(st.empty()||st.peek()!='{')
                    {
                    result=false;
                    break;
                }
                st.pop();
            }
            else if(s[i]==']')
                {
                if(st.empty()||st.peek()!='[')
                    {
                    result=false;
                    break;
                }
                st.pop();
            }
        }
        if(result==true && st.empty())
            System.out.println("YES");
        else 
            System.out.println("NO");

    }

    }
}
