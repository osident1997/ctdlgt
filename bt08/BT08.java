public class BT08{
Node MergeLists(Node headA, Node headB) {
     // This is a "method-only" submission. 
     // You only need to complete this method 
   if((headA==null)&&(headB==null))
    return null;
    if((headA!=null)&&(headB==null))
    return headA;
    if((headA == null)&&(headB!=null))
    return headB;
    if(headA.data < headB.data)
    headA.next = MergeLists(headA.next, headB);
    else if(headA.data > headB.data)
        {
        Node temp = headB;
        headB = headB.next;
        temp.next = headA;
        headA = temp;
        headA.next = MergeLists(headA.next, headB);
}
return headA;

}

Node RemoveDuplicates(Node head) {
  // This is a "method-only" submission. 
  // You only need to complete this method.
    if ( head == null ) return null;
  Node nextItem = head.next;
  while ( nextItem != null && head.data == nextItem.data ) {
    nextItem = nextItem.next;
  }
  head.next = RemoveDuplicates( nextItem );
  return head;
}

boolean hasCycle(Node head) {
    if (head == null){
        return false;
    }

    Node slow = head;
    Node fast = head;

    while (fast != null && fast.next != null){
        slow = slow.next;
        fast = fast.next.next;

        if (slow == fast){
            return true;
        }
    }

    return false;
}

Node SortedInsert(Node head,int data) {
  Node n = new Node();
    n.data = data;
    if (head == null) {
        return n;
    }
    else if (data <= head.data) {
        n.next = head;
        head.prev = n;
        return n;
    }
    else {
        Node rest = SortedInsert(head.next, data);
        head.next = rest;
        rest.prev = head;
        return head;
    }
}

Node Reverse(Node head) {
 Node temp = head;
    Node newHead = head;
    while (temp != null) {
        Node prev = temp.prev;
        temp.prev = temp.next;
        temp.next = prev;
        newHead = temp;
        temp = temp.prev;
    }
    return newHead;
}
}